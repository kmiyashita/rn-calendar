import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import rootReducer from './src/modules';

import { StyleSheet, View} from 'react-native';
import Calendar from './src/components/Calendar';
import Navi from './src/components/Navi';
import Schedule from './src/components/Schedule';

const store = createStore(rootReducer,
                          composeWithDevTools(applyMiddleware(thunk)));

export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Navi/>
          <Calendar/>
          <Schedule/>
        </View>
      </Provider>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff'
  },
});
