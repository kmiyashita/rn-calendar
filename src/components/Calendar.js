import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';

class Calendar extends React.Component {
  render() {
    const dayOfWeekNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    const year = 2018;//this.props.navi.year;
    const month = 7;//this.props.navi.month;
    const dayOfWeek1st = (new Date(year, month, 1)).getDay();
    const numOfDays = this.calcNumOfDaysInMonth(year, month);

    return(
      <View  style={styles.container}>
        <Table borderStyle={{borderColor: 'transparent'}}>
          {/* Sun, Mon, ... */}
          <Row data={dayOfWeekNames} style={styles.dayOfWeek} textStyle={styles.text}/>
          {/* weeks */}
          {this.renderWeek(0, dayOfWeek1st, numOfDays)}
          {this.renderWeek(1, dayOfWeek1st, numOfDays)}
          {this.renderWeek(2, dayOfWeek1st, numOfDays)}
          {this.renderWeek(3, dayOfWeek1st, numOfDays)}
          {this.renderWeek(4, dayOfWeek1st, numOfDays)}
          {this.renderWeek(5, dayOfWeek1st, numOfDays)}
        </Table>
      </View>
    );
  }

  /**
    * render a row of a week.
    *
    * @param {int} weekIdx       the row index starting from 0
    * @param {int} dayOfWeek1st  what day of the week is the 1st day of
    *                            this month? (0:Sun, 1:Mon, 2:Tue ...)
    * @param {int} numOfDays     the number of days in this month.
    */
  renderWeek(weekIdx, dayOfWeek1st, numOfDays){
    var dateSun = weekIdx * 7 + 1 - dayOfWeek1st;
    var dateArray = []
    for(var i = 0; i < 7; i++){
      // which day of the month? 0 means invalid.
      var date = dateSun + i;
      if(date < 0){
        date = 0;
      }else if(date > numOfDays){
        date = 0;
      }
      // create the event indicator
      var eventNum = 0;
      // if(this.props.navi.events.length > 0){
      //   eventNum = this.props.navi.events[date].length;
      // }
      eventNum = 2;

      var eventIndicator;
      if(date === 0){
        eventIndicator = '';
      }else if(eventNum === 0){
        eventIndicator = '-';
      }else if(eventNum === 1){
        eventIndicator = '*';
      }else if(eventNum === 2){
        eventIndicator = '**';
      }else{
        eventIndicator = '***';
      }

      var selected = ((date !== 0) &&
                      (date === 3 /*this.props.navi.dayOfMonth*/))
                      ? true : false;
      dateArray.push(<DateTile key={i} date={date} selected={selected}
                                eventIndicator={eventIndicator}
                                onClick={this.popupAlert}
                                /* onClick={this.props.selectDate} */ />);
    }
    return (
      <TableWrapper key={weekIdx} style={styles.row}>
        {dateArray}
      </TableWrapper>
    );
  }

  /**
    * Calculate the number of days in a certain month.
    *
    * @param {int} year   e.g) 2018
    * @param {int} month  0: Jan, 1: Feb, ...
    *
    * @return {int} numOfDays
    */
  calcNumOfDaysInMonth(year, month){
    if(month === 3 || month === 5 || month === 8 || month === 10){
      return 30;
    }else if(month === 2){
      if(year % 4 === 0){
        // leap year
        return 29;
      }else{
        return 28;
      }
    }else{
      return 31;
    }
  }

  popupAlert(num){
    Alert.alert('Num = ' + num);
  }
}

/**
  * @param {int} date  the day of the month (1-31) or 0 when empty.
  * @param {bool} selected  true if this tile is selected
  * @param {string} eventIndicator   string showing events e.g.) '***'
  * @param {function(int)} onClick   callback to call when a DateTile is clicked.
  */
const DateTile = (props) => {
  const styleBg = (props.selected) ? styles.tileSelected : styles.tile;

  const touchable = (
    <TouchableOpacity style={styleBg} onPress={() => props.onClick(props.date)}>
      <View>
        <Text>
          {(props.date === 0) ? ' ' : '' + props.date + '\n' + props.eventIndicator}
        </Text>
      </View>
    </TouchableOpacity>);

  return(
    <Cell data={touchable}/>
  );
}

const styles = StyleSheet.create({
  container: { flex: 20, padding: 16, paddingTop: 30, backgroundColor: '#ffffff' },
  dayOfWeek: { height: 40, backgroundColor: '#808B97' },
  text: { margin: 6 },
  row: { flexDirection: 'row'},
  tile: {},
  tileSelected: {backgroundColor: '#aaaaaa'}
});


export default Calendar;
