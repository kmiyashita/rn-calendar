import React from 'react'
import { Alert, AppRegistry, Button, StyleSheet, View, Text } from 'react-native';

class Navi extends React.Component {
  componentDidMount() {
    // this.props.initGoogleApi();
  }

  render() {
    let signInOutButton;
    if(true /* this.props.navi.isSignedIn */){
      signInOutButton = (<Button onPress={this.props.signOut} title='Sign Out'/>);
    }else{
      signInOutButton = (<Button onPress={this.props.signIn} title='Sign In'/>);
    }

    return (
      <View style={styles.container}>
        <Button style={styles.button} onPress={this.props.prevMonth} title='Prev'/>
        <Text style={styles.button}>{'' + 2018/*this.props.navi.year*/ + '-' + (7/*this.props.navi.month*/ + 1)}</Text>
        <Button style={styles.button} onPress={this.props.nextMonth} title='Next'/>
        {signInOutButton} 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flexDirection: 'row', flex: 1, justifyContent: 'space-between',
                backgroundColor: '#ffffff'},
  button: {height: 20}
});

export default Navi;
